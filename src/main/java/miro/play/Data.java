/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package miro.play;

/**
 *
 * @author miroslav
 */
public class Data implements Speaking {

    private String salutation = "Data speaking";

    /**
     * Get the value of salutation
     *
     * @return the value of salutation
     */
    public String getSalutation() {
        return salutation;
    }

    /**
     * Set the value of salutation
     *
     * @param salutation new value of salutation
     */
    public void setSalutation(String salutation) {
        this.salutation = salutation;
    }

    public Data(String salutation) {
    }

    public Data() {
    }
    
    @Override
    public String speak() {
        if (salutation != null) {
            return salutation;
        } else {
            return "Can't speak";
        }
    }

}
